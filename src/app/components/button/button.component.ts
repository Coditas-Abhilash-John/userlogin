import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  constructor() { }
  @Input() buttonLabel : string = "";
  @Input() type : string = "";
  @Output() buttonClicked = new EventEmitter();
  ngOnInit(): void {
  }
  onClick(){
    this.buttonClicked.emit();
  }
}
