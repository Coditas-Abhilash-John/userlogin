import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input-tile',
  templateUrl: './input-tile.component.html',
  styleUrls: ['./input-tile.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputTileComponent,
      multi: true,
    },
  ],

})
export class InputTileComponent implements OnInit,ControlValueAccessor{

  onChange!: (value: string) => void;
  @Input() type : string = ""
  @Input() label =""
  inputValue :string = "";

  constructor() { }
  writeValue(obj: any): void {
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
  }
  ngOnInit(): void {
  }

}
