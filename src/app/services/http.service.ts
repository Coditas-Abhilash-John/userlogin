import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  IUserLoginCredentials,
  IUserRegisterCredentials,
} from '../model/userLoginInterface';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  private baseUrl = 'https://forms-47.herokuapp.com/';

  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private httpClient: HttpClient) {}

  post(endPoint: string,data: IUserRegisterCredentials | IUserLoginCredentials) {
    return this.httpClient.post(
      this.baseUrl + endPoint,
      JSON.stringify(data),
      this.httpHeader
    );
  }

  registerUser(userRegisterData: IUserRegisterCredentials): Observable<any> {
    return this.post('user/register', userRegisterData);
  }

  logUser(userLoginData: IUserLoginCredentials): Observable<any> {
    return this.post('user/login', userLoginData);
  }

  getQuestion(token: string): Observable<any> {
    return this.httpClient.get(this.baseUrl + 'question', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    });
  }
}
