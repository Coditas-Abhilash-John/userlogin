import { Component, DoCheck, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit,OnChanges{

  questions = "";
  constructor( private httpService : HttpService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['isUserLoggedIn'].currentValue && window.localStorage.getItem("token")){
      this.fetchQuestion();
    }
  }
  @Input() isUserLoggedIn = false;
  ngOnInit(): void {
    this.reset();
  }

  reset(){
    this.questions = "No Content"
    if(window.localStorage.getItem("token")){
      this.fetchQuestion();
    }
  }

  fetchQuestion(){
      this.httpService.getQuestion(localStorage.getItem('token')!).subscribe(
        value=>{
          this.questions = value.data.questions
        }
      )
  }

  logOut(){
    window.localStorage.removeItem("token")
    this.isUserLoggedIn = false;
    this.reset();
  }
}
