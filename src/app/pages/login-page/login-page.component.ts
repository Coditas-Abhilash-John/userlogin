import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IUserLoginCredentials } from 'src/app/model/userLoginInterface';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  constructor( private httpService : HttpService) { }

  @Output() login = new EventEmitter();

  ngOnInit(): void {
  }
  userLoginForm = new FormGroup({
    userEmail : new FormControl("",[Validators.required,Validators.email]),
    password : new FormControl("",[Validators.required])
  })

  userLogin(userLoginData :IUserLoginCredentials){
    this.httpService.logUser(userLoginData).subscribe(
      value=>{
        alert("User Login Successful");
        window.localStorage.setItem("token", value.data.token);
        this.login.emit();
      }
    )
  }

  onLogin(userLoginInfo : FormGroup){
    if(!userLoginInfo.invalid){
      this.userLogin({
        email : userLoginInfo.value.userEmail,
        password : userLoginInfo.value.password
      })
    }
  }

}
