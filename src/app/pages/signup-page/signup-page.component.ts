import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IUserRegisterCredentials } from 'src/app/model/userLoginInterface';
import { HttpService } from 'src/app/services/http.service';
@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss']
})
export class SignupPageComponent implements OnInit {

  constructor( private httpService :HttpService) { }

  ngOnInit(): void {
  }

  userSignUpForm = new FormGroup({
    name : new FormControl('',[Validators.required,Validators.minLength(3)]),
    email : new FormControl('',[Validators.required,Validators.email]),
    password : new FormControl('',[Validators.required]),
    confirmPassword : new FormControl('',[Validators.required])
  })

  registerNewUser(userSignUpData:IUserRegisterCredentials){
    this.httpService.registerUser(userSignUpData).subscribe(
      value=>{alert(value);
      console.log(value);
      }
    )
  }

  onSignUp(userSignUpData :  FormGroup ){
    if(userSignUpData.valid){
      this.registerNewUser({
        ...userSignUpData.value,
        role : "6299b4ffe3d2004c0a545c32"
      })
  }
}
}


