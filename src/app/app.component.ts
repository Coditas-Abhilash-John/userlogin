import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit{

  title = 'userLogin';

  token: string = '';
  isUserLoggedIn = false

  ngOnInit(): void {
    this.isUserLoggedIn = window.localStorage.getItem("token") ?true :false;
  }
  onUserLogin(){
    this.isUserLoggedIn = true;
  }


}
