export interface IUserLoginCredentials{
  email : string,
  password : string
}

export interface IUserRegisterCredentials{
  name : string,
  email : string,
  password : string,
  confirmPassword : string,
  role : string
}

